Server component
==============

Python Server implemented for a hybrid smartphone-server process mining architecture This component fulfills the functions of the Social Process Mining component and the Aggregator.

If this code is used, reference the original article:

*[reference not yet available]*


Requirements
------

This server uses Flask for communication with the other component of the architecture and PM4PY for process mining.

To install Flask on your computer, you need to have Python and PIP installed previously. After that, run `pip install flask`

To install PM4PY, follow the tutorial on the official website: https://pm4py.fit.fraunhofer.de/install (you need to complete the optional step of installing GraphViz to save the models as an image).

