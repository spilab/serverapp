import os
from pm4py.objects.log.importer.xes import factory as xes_importer

from pm4py.visualization.heuristics_net import factory as hn_vis_factory
from pm4py.algo.discovery.heuristics import factory as heuristics_miner
from pm4py.evaluation import factory as evaluation_factory

from xes_utils import combine_logs
import time
import numpy as np

os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin/'

DATA = "data"
OUTPUT_FOLDER="model"
TOTAL_LOG_LOGNAME = "log_total.xes"

def read_query_folder(query):
    query_path = os.path.join(DATA, query)
    logs = os.listdir(query_path)
    print(logs)
    if not os.path.exists(os.path.join(OUTPUT_FOLDER, query)):
        os.makedirs(os.path.join(OUTPUT_FOLDER, query))
    if str(TOTAL_LOG_LOGNAME) in logs:
        logs.remove(str(TOTAL_LOG_LOGNAME))
    return query_path, logs


def generate_log_total(path, logs):
    time_init = time.time()
    logs_paths = []
    for log in logs :
        logs_paths.append(os.path.join(path,log))
    combine_logs(logs_paths, path, str(TOTAL_LOG_LOGNAME))
    print("Generating total log with ",str(len(logs))," logs")
    return time.time() - time_init


def generate_model(query, logname):
    time_log_init = time.time()
    log = xes_importer.import_log(os.path.join(DATA, query, logname))
    print(str(logname) + " - Number of traces:", len(log))

    heu_net = heuristics_miner.apply_heu(log, parameters={"dependency_thresh": 0.5})
    parameters = {"format": "png"}
    gviz = hn_vis_factory.apply(heu_net, parameters=parameters)
    hn_vis_factory.save(gviz, os.path.join(str(OUTPUT_FOLDER), str(query), str(logname).replace("xes", "png")))
    time_model = time.time() - time_log_init

    net, im, fm = heuristics_miner.apply(log, parameters={"dependency_thresh": 0.5})
    evaluation_model = evaluate(log, net, im, fm)
    number_arcs =len(heu_net.dfg.items())
    number_act = len(heu_net.activities)
    sum_arcs = get_occurences(heu_net.dfg)

    return time_model, evaluation_model, number_arcs, number_act, sum_arcs


def generate_individual_models(query, logs):
    time_init = time.time()
    times_peer_log = []
    evaluations_logs = []
    number_arcs = []
    number_act = []
    sum_arcs = []
    for logname in logs:
        time_model, evaluation_model, n_arcs, n_act, s_arcs = generate_model(query, logname)

        times_peer_log.append(time_model)
        evaluations_logs.append(evaluation_model)
        number_arcs.append(n_arcs)
        number_act.append(n_act)
        sum_arcs.append(s_arcs)
    sum_times = time.time() - time_init

    return number_act, number_arcs, sum_arcs, evaluations_logs, sum_times, times_peer_log


def generate_total_log_model(query):
    time_model, evaluation_model, n_arcs, n_act, s_arcs = generate_model(query, TOTAL_LOG_LOGNAME)
    return n_act, n_arcs, s_arcs, evaluation_model, time_model


def evaluate(log, net, ini_mark, fin_mark):
    evaluation_result = evaluation_factory.apply(log, net, ini_mark, fin_mark)
    return evaluation_result


def get_occurences(arcs):
    total = 0
    for arc in arcs:
        total = total + arcs[arc]
    return total


def show_statistics(times):
    for query in times:
        print(query)
        for metric in times[query]:
            print(metric, ':', times[query][metric])







if not os.path.exists(OUTPUT_FOLDER):
    os.makedirs(OUTPUT_FOLDER)

data_contents = os.listdir(DATA)
print(data_contents)
total_times = {}
for query in data_contents:
    if os.path.isdir(os.path.join(DATA,query)):
        query_path, logs = read_query_folder(query)
        query_stats = {}
        time_generate_log_total = generate_log_total(query_path, logs)
        _, _, _, _, time_individual_models, times_logs = generate_individual_models(query,logs)
        number_act, number_arcs, sum_arcs, evaluations_logs, time_total_log_model = generate_total_log_model(query)

        query_stats["activities"] = number_act
        query_stats["arcs"] = number_arcs
        query_stats["sum_arcs_weights"] = sum_arcs
        query_stats["evaluation"] = evaluations_logs
        query_stats["generate_log_total"] = time_generate_log_total
        query_stats["time_total_log_model"] = time_total_log_model
        query_stats["sum_time_individual_models"] = time_individual_models
        query_stats["avg_time_individual_models"] = np.average(np.array(times_logs))

        total_times[query] = query_stats

    show_statistics(total_times)


