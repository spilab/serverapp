from flask import Flask, request
from xes_utils import save_traces
import os

BASE_DIR = "data"


def create_dir(dirs):
    path = BASE_DIR
    for dir in dirs:
        path = os.path.join(path,dir)
    if not os.path.exists(path):
        os.makedirs(path)
    return path

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/traces', methods=["POST"])
def get_traces():
    body = request.get_json()

    username = body["username"]
    pattern = body["pattern"]
    dir = create_dir([pattern])
    traces = body["traces"]

    saved = save_traces(dir, username, traces)
    if saved:
        return "", 201
    else:
        return "", 500


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8080)
