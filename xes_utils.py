import os
import xes_template as xt
from xml.dom import minidom
import xml.etree.ElementTree as ET


def save_traces(dir, filename, traces):

    traces_events = []
    for index, trace in enumerate(traces):
        events = []
        for event in trace:
            event = str(event).split(" ")
            lifecycle = event[-1]
            name = " ".join(event[:-1])
            e = xt.event_template
            e = e.replace(xt.EVENT_NAME_KEY,name)
            e = e.replace(xt.EVENT_STATUS_KEY, lifecycle)
            events.append(e)
        events_string = "\n".join(events)
        t = xt.trace_template
        t = t.replace(xt.NUMBER_TRACE_KEY, "case"+str(index))
        t = t.replace(xt.EVENTS_KEY, events_string)
        traces_events.append(t)
    traces_string = "\n".join(traces_events)

    xes_content = xt.xes_template
    xes_content = xes_content.replace(xt.TRACES_KEY,traces_string)

    filename.strip()
    if not filename.endswith(".xes"):
        filename = str(filename)+".xes"

    with open(os.path.join(dir, filename), "w") as xes_file:
        xes_file.write(xes_content)

    return True



def combine_logs(dir_logs, dir_output, name_output):
    traces_total = []
    for dir in dir_logs:
        tree = ET.parse(dir)
        root = tree.getroot()
        for trace in root.iter('trace'):
            print(trace)
            trace_events = []
            for event in trace.iter('event'):
                event_string = " "
                values = event.findall('string')
                for value in values:
                    if value.attrib["key"] == "lifecycle:transition":
                        event_string = str(event_string) + str(value.attrib["value"])
                    if value.attrib["key"] == "concept:name":
                        event_string = str(value.attrib["value"]) + str(event_string)
                print(event_string)
                trace_events.append(event_string)
            traces_total.append(trace_events)

    print(traces_total)
    save_traces(dir_output, name_output, traces_total)


