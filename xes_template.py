xes_template = """<?xml version='1.0' encoding='UTF-8'?>
<log>
  <string key="concept:name" value="DataUser5.xes"/>
  <extension name="Concept" prefix="concept" uri="http://www.xes-standard.org/concept.xesext"/>
  <extension name="Lifecycle" prefix="lifecycle" uri="http://www.xes-standard.org/lifecycle.xesext"/>
  $$$$TRACES$$$$
</log>
"""

trace_template = """
  <trace>
    <string key="concept:name" value="$$$$NUMBER_TRACE$$$$"/>
    $$$$EVENTS$$$$
  </trace>"""

event_template = """
     <event>
      <string key="concept:name" value="$$$$EVENT_NAME$$$$"/>
      <string key="lifecycle:transition" value="$$$$EVENT_STATUS$$$$"/>
    </event>"""


TRACES_KEY = "$$$$TRACES$$$$"
NUMBER_TRACE_KEY = "$$$$NUMBER_TRACE$$$$"
EVENTS_KEY = "$$$$EVENTS$$$$"
EVENT_NAME_KEY = "$$$$EVENT_NAME$$$$"
EVENT_STATUS_KEY = "$$$$EVENT_STATUS$$$$"